package com.lancasda.tvshowservice.dto;

import com.lancasda.tvshowservice.domain.TvShow;

import java.util.List;

public class TvShowMessage {
    private List<TvShow> payload;
    private int skip;
    private int take;
    private int totalRecords;

    public List<TvShow> getPayload() {
        return payload;
    }

    public void setPayload(List<TvShow> payload) {
        this.payload = payload;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getTake() {
        return take;
    }

    public void setTake(int take) {
        this.take = take;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

}
