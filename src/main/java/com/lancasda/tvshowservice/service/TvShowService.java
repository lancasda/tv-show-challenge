package com.lancasda.tvshowservice.service;

import com.lancasda.tvshowservice.domain.TvShow;
import com.lancasda.tvshowservice.domain.TvShowSummary;

import java.util.List;

public interface TvShowService {

    List<TvShowSummary> getDrmShows(List<TvShow> tvShows);

}
