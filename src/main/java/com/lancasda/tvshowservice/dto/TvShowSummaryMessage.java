package com.lancasda.tvshowservice.dto;

import com.lancasda.tvshowservice.domain.TvShowSummary;

import java.util.List;

public class TvShowSummaryMessage {
    private List<TvShowSummary> response;

    public TvShowSummaryMessage() {
        super();
    }

    public TvShowSummaryMessage(List<TvShowSummary> showSummaries) {
        this.response = showSummaries;
    }

    public List<TvShowSummary> getResponse() {
        return response;
    }

    public void setResponse(List<TvShowSummary> response) {
        this.response = response;
    }
}
