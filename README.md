# Simple TV Show Web Service

This is a simple RESTful web service to retrieve information about TV shows.

It is implemented in Java using features of the Spring and Spring Boot Framework.

To get started, import the Maven `pom.xml` into your favourite IDE (this may require installing a Maven plugin),
and run the main method from the `TvShowWebService` class.

See the various `application-*.properties` files in the `src/main/resources` directory for 
environment configurations, including the port the service listens on.  Different environments
can be selected by providing the correct environment name as a Spring profile at startup (e.g. `-Dspring.profiles.active=dev`).

## Endpoints

Only one endpoint exists currently.

### "/"

The root endpoint accepts a JSON request body containing a list of TV shows, and 
returns a JSON array containing a summary of the TV shows which are DRM enabled and contain one or 
more episodes.

Sample JSON input and output can be found in the `src/test/resources` directory.

---

## Code

The code is organised into controller, domain, DTO, and service packages.  See below for a short
description of each.

#### Controller

REST controller classes to handle external requests.

#### Domain

Domain classes to represent the data internally to the service.

#### DTO

Data transfer objects which provide a view of the data sent to and from the client.

#### Service

Services to provide logic for processing internal domain objects.
