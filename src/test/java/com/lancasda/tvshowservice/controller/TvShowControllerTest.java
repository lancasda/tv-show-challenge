package com.lancasda.tvshowservice.controller;

import com.lancasda.tvshowservice.ResourceReader;
import com.lancasda.tvshowservice.domain.TvShow;
import com.lancasda.tvshowservice.domain.TvShowSummary;
import com.lancasda.tvshowservice.dto.TvShowMessage;
import com.lancasda.tvshowservice.dto.TvShowSummaryMessage;
import com.lancasda.tvshowservice.service.TvShowService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TvShowController.class)
public class TvShowControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TvShowService tvShowService;

    private static final String GOOD_REQUEST_JSON_FILE = "good_request.json";
    private static final String GOOD_RESPONSE_JSON_FILE = "good_response.json";
    private static final String BAD_REQUEST_JSON_FILE = "bad_request.json";
    private static final String BAD_RESPONSE_ERROR_FILE = "bad_response.json";

    @Test
    public void testGetDrmShows() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        String requestJson = ResourceReader.getStringFromResource(GOOD_REQUEST_JSON_FILE);
        List<TvShow> tvShowList = mapper.readValue(requestJson, TvShowMessage.class).getPayload();

        String responseJson = ResourceReader.getStringFromResource(GOOD_RESPONSE_JSON_FILE);
        List<TvShowSummary> tvShowSummaryList = mapper.readValue(responseJson, TvShowSummaryMessage.class).getResponse();

        given(tvShowService.getDrmShows(tvShowList)).willReturn(tvShowSummaryList);

        String response = mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        JSONAssert.assertEquals(responseJson, response, JSONCompareMode.STRICT);
    }

    @Test
    public void testBadRequest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        String requestJson = ResourceReader.getStringFromResource(BAD_REQUEST_JSON_FILE);
        String responseJson = ResourceReader.getStringFromResource(BAD_RESPONSE_ERROR_FILE);

        String response = mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn()
                .getResponse()
                .getContentAsString();

        JSONAssert.assertEquals(responseJson, response, JSONCompareMode.STRICT);
    }
}
