package com.lancasda.tvshowservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvShowWebService {

	public static void main(String[] args) {
		SpringApplication.run(TvShowWebService.class, args);
	}

}
