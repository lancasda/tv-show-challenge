package com.lancasda.tvshowservice.controller;

public class ControllerError {

    private final String error;

    public ControllerError(String error) {
        super();
        this.error = error;
    }

    public String getError() {
        return error;
    }
}