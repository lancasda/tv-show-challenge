package com.lancasda.tvshowservice.service;

import com.lancasda.tvshowservice.domain.TvShow;
import com.lancasda.tvshowservice.domain.TvShowSummary;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DefaultTvShowServiceTest {

    private TvShow getDefaultShow() {
        TvShow tvShow = new TvShow();
        tvShow.setSlug("TvShowSlug" + Math.random() * 1000);
        tvShow.setTvChannel("Channel 9");
        return tvShow;
    }

    @Test
    public void testGetDrmShows() {
        List<TvShow> allTvShows = new ArrayList<>();
        TvShow tvShow = getDefaultShow();
        tvShow.setDrm(false);
        tvShow.setEpisodeCount(10);
        allTvShows.add(tvShow);
        tvShow = getDefaultShow();
        tvShow.setDrm(false);
        tvShow.setEpisodeCount(0);
        allTvShows.add(tvShow);
        tvShow = getDefaultShow();
        tvShow.setDrm(true);
        tvShow.setEpisodeCount(10);
        tvShow.setTitle("DRM TvShow");
        allTvShows.add(tvShow);
        tvShow = getDefaultShow();
        tvShow.setDrm(true);
        tvShow.setEpisodeCount(0);
        allTvShows.add(tvShow);

        DefaultTvShowService service = new DefaultTvShowService();
        List<TvShowSummary> result = service.getDrmShows(allTvShows);

        Assert.assertEquals(1, result.size());
        Assert.assertEquals("DRM TvShow", result.get(0).getTitle());
    }
}
