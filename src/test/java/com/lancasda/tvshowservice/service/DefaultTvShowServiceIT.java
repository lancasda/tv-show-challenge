package com.lancasda.tvshowservice.service;

import com.lancasda.tvshowservice.ResourceReader;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DefaultTvShowServiceIT {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders httpHeaders;
    private static final String REQUEST_JSON_FILE = "good_request.json";
    private static final String RESPONSE_JSON_FILE = "good_response.json";

    @Before
    public void setUp() {
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Type", "application/json");
        httpHeaders.set("Accept", "application/json");
    }

    @Test
    public void testGetDrmShows() throws JSONException, IOException {
        String requestJson = ResourceReader.getStringFromResource(REQUEST_JSON_FILE);
        HttpEntity<String> httpEntity = new HttpEntity<>(requestJson, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                "http://localhost:" + port + "/", httpEntity, String.class);

        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        JSONAssert.assertEquals(
                ResourceReader.getStringFromResource(RESPONSE_JSON_FILE),
                responseEntity.getBody(),
                JSONCompareMode.STRICT);
    }

}
