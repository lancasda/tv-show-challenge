package com.lancasda.tvshowservice;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ResourceReader {

    private ResourceReader() {
        super();
    }

    public static String getStringFromResource(String fileName) throws IOException {
        ClassLoader classLoader = ResourceReader.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        byte[] fileBytes = Files.readAllBytes(file.toPath());
        return new String(fileBytes).trim();
    }
}
