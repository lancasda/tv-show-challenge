package com.lancasda.tvshowservice.service;

import com.lancasda.tvshowservice.domain.TvShow;
import com.lancasda.tvshowservice.domain.TvShowSummary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultTvShowService implements TvShowService {

    @Override
    public List<TvShowSummary> getDrmShows(List<TvShow> tvShows) {
        return tvShows.stream()
                .filter(show -> show.isDrm() && show.getEpisodeCount() > 0)
                .map(TvShowSummary::new)
                .collect(Collectors.toList());
    }

}
