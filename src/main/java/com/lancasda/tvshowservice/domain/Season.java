package com.lancasda.tvshowservice.domain;

public class Season {

    private String slug;

    public Season() {
        super();
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
