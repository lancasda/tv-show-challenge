package com.lancasda.tvshowservice.controller;

import com.lancasda.tvshowservice.domain.TvShowSummary;
import com.lancasda.tvshowservice.domain.TvShow;
import com.lancasda.tvshowservice.dto.TvShowMessage;
import com.lancasda.tvshowservice.dto.TvShowSummaryMessage;
import com.lancasda.tvshowservice.service.TvShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class TvShowController {

    @Autowired
    private TvShowService tvShowService;

    @ResponseBody
    @RequestMapping(
            value = "",
            method = RequestMethod.POST,
            consumes = { "application/json" },
            produces = { "application/json" })
    public TvShowSummaryMessage getDrmShows(@Valid @RequestBody TvShowMessage o) {
        List<TvShow> allTvShows = o.getPayload();
        List<TvShowSummary> drmShows = tvShowService.getDrmShows(allTvShows);
        return new TvShowSummaryMessage(drmShows);
    }

}
