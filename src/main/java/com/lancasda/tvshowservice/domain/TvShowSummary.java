package com.lancasda.tvshowservice.domain;

import java.util.Objects;

public class TvShowSummary {

    private String image;
    private String slug;
    private String title;

    public TvShowSummary() {
        super();
    }

    public TvShowSummary(TvShow tvShow) {
        this(tvShow.getImage() == null ? null : tvShow.getImage().getShowImage(), tvShow.getSlug(), tvShow.getTitle());
    }

    public TvShowSummary(String image, String slug, String title) {
        this.image = image;
        this.slug = slug;
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TvShowSummary that = (TvShowSummary) o;
        return Objects.equals(slug, that.slug);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slug);
    }
}
